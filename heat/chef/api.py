import paramiko
import re
import subprocess
import chef
import chef.api as ChefAPI
import chef.node as ChefNode
import chef.data_bag as ChefDataBag
import chef.exceptions as ChefException

from oslo.config import cfg
from heat.common import exception
from heat.openstack.common import log as logging

logger = logging.getLogger(__name__)

def get_api():
    return chef.autoconfigure()

def trigger_chef_client(hostname, port=22):
#    hostname = hostname + '.novalocal'
    hostname = hostname.lower()
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    pt_hostname = re.compile('(https:\/\/)([\s\S]+)(:)')
    match = re.search(pt_hostname, get_api().url)
    if match:
        ssh.connect(match.group(2), port=port, username="root")
        command = "killall -USR1 chef-client"
        # knife ssh name:au-luster-olzqrjl66r6a-a26me4t53j7j.novalocal -a ipaddress -x root 'killall -USR1 chef-client' > knife_ssh.log
        ssh.exec_command("knife ssh name:" + hostname + " -a ipaddress -x root \'"
                         + command + "\' > knife_ssh.log")
        return True
    return False

def check_chef_complete(ip):
    ssh = subprocess.Popen(['sshpass', 'ssh', 'root@'+ ip, 'cat', '/root/chef-client.log'], stdout=subprocess.PIPE)

    for line in ssh.stdout.readlines():
        match = re.search(r'Report handlers', line)
        if match:
            return True
    return False

def client_delete(name):
    api = get_api()
    name = name.lower()
    url = '/clients'
    try:
        #api.api_request('DELETE', url + '/' + name + '.novalocal')
        api.api_request('DELETE', url + '/' + name)
        return True
    except Exception:
        logger.info(_('Chef client %s can not delete') %name)
    return False

def node_delete(name):
    api = get_api()
    name = name.lower()
    try:
        node = chef.Node(name, api)
        #node = chef.Node(name+'.novalocal', api)
        node.delete()
        return True
    except Exception:
        logger.info(_('Chef Node %s can not delete') %name)
    return False

def databag_create(databag_name):
    api = get_api()
    url = '/data'
    body = { "name" : databag_name }
    try:
        db = api.api_request('POST', url, data=body)
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag create error'))
        db = chef.DataBag(databag_name)
    return db

def databag_item_create(databag_name, item_name):
    api = get_api()
    try:
        db_item = chef.DataBagItem.create(databag_name, item_name, api)
    except ChefException.ChefServerError:
        logger.info(_('ChefServer DataBag item create error'))
        db_item = ""

    return db_item

def databag_get(databag_name, item_name):
    api = get_api()
    databag = chef.DataBag(databag_name, api)
    return databag[item_name]

def databag_delete(databag_name):
    api = get_api()
    try:
        databag = chef.DataBag(databag_name, api)
        databag.delete()
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag delete error'))

def databag_item_list_update(databag_name, item_name, key, value):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    try:
        #data = dict(api.api_request('GET', url))
        data = _databag_item_list(databag_name, item_name)
        try:
            if key in data.keys():
                data[key].append(value)
            else:
                data.update({key:[value]})
        except KeyError:
            logger.info(_('ChefServer Databag item host no key %(key)s'
                           'key create %(key)s')%{'key':key})

        api.api_request('PUT', url, data=data)

    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item update error'))    

def databag_item_update(databag_name, item_name, key, value):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    try:
        #data = dict(api.api_request('GET', url))
        data = _databag_item_list(databag_name, item_name)
        try:
            if key in data.keys():
                data[key] = value
            else:
                data.update({key:value})
        except KeyError:
            logger.info(_('ChefServer Databag item host no key %(key)s'
                           'key create %(key)s')%{'key':key})

        api.api_request('PUT', url, data=data)

    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item update error'))

def _databag_item_list(databag_name, item_name):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    try:
        data = dict(api.api_request('GET', url))
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item list error'))    
        data = {}
    return data

def databag_item_delete(databag_name, item_name, key, value):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    try:
        data = _databag_item_list(databag_name, item_name)
        if key in data.keys():
            for i in reversed(range(len(data[key]))):
                if data[key][i] == value:
                    data[key].pop(i)

        api.api_request('PUT', url, data=data)
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item delete error'))

def databag_init_node_get(databag, item_name):
    api = get_api()
    try:
        db = _databag_item_list(databag, item_name)
        return db['init_node']
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item list error'))
        
    return ""

def databag_init_host_get(databag, item_name):
    api = get_api()
    try:
        db = _databag_item_list(databag, item_name)
        return db['init_host']
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item list error'))
        
    return ""
    
# it should be not here...
# add ip, name, is_master into 'nodes'
#def databag_register_node(databag_name, item_name, ip, name):
#    data = { "name" : name , 
#             "ip" : ip }
#    databag_item_list_update(databag_name, item_name, databag_name + '_nodes_ext', data)
#    databag_item_list_update(databag_name, item_name, databag_name + '_nodes', ip)
def get_host_ip_from_databag(databag_name, item_name, name):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    key = databag_name + '_nodes_ext'
    #neutron
    #name = name + '.novalocal'
    try:
        data = _databag_item_list(databag_name, item_name)
        if key in data.keys():
            for i in reversed(range(len(data[key]))):
                if data[key][i]['name'] == name:
                    return data[key][i]['ip']
    except ChefException.ChefServerError:
        logger.info(_('ChefServer Databag item get error'))
    return ""

def databag_register_node(databag_name, item_name, ip, name):
    #data = { "name" : name + '.novalocal' , 
    data = { "name" : name,
             "ip" : ip }
    databag_item_list_update(databag_name, item_name, databag_name + '_nodes_ext', data)
    databag_item_list_update(databag_name, item_name, databag_name + '_nodes', ip)


# 
def databag_delete_node(databag_name, item_name, ip, name):
    data = { "name" : name ,
    #data = { "name" : name + '.novalocal',
             "ip" : ip }
    databag_item_delete(databag_name, item_name, databag_name + '_nodes_ext', data)
    databag_item_delete(databag_name, item_name, databag_name + '_nodes', ip)

